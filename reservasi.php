<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Kegunung Indonesia - Reservasi Gunung</title>
    <?php include "partition/header.php" ?>
    <link rel="stylesheet" href="css/reservasi.css">
</head>
<body>

<?php include "partition/navbar.php" ?>

<?php
if (!$sessionHandler->isRegistered()) {
    header('Location: login.php');
    return;
}

if (!isset($_POST['btnCheckout'])) {
    header('Location: detail-gunung.php');
    return;
}

include "connection.php";

$inGunung = mysqli_real_escape_string($conn, $_POST['gunung']);
$inNaik = mysqli_real_escape_string($conn, $_POST['naik']);
$inTurun = mysqli_real_escape_string($conn, $_POST['turun']);
$inJumlah = mysqli_real_escape_string($conn, $_POST['jumlah']);

if ($inJumlah > 4) {
    header('Location: detail-gunung.php');
    return;
}

$sessionHandler->set('inGunung', $inGunung);
$sessionHandler->set('inNaik', $inNaik);
$sessionHandler->set('inTurun', $inTurun);
$sessionHandler->set('inJumlah', $inJumlah);

?>

<form action="proses/proses-reservasi.php" method="post">
    <div class="row my-5">
        <div class="col-12">
            <h1 class="font-weight-bold text-center">Form Identitas Organisasi</h1>
            <div class="box">
                <div class="form-group">
                    <input name="namaOrganisasi" type="text" class="form-control" placeholder="Nama Organisasi">
                </div>
                <div class="form-group">
                <textarea name="alamatOrganisasi" rows="8" cols="80" class="form-control"
                          placeholder="Alamat Organisasi"></textarea>
                </div>
                <div class="form-group">
                    <input name="telOrganisasi" type="text" class="form-control" placeholder="Nomor Telepon Organisasi">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <h1 class="font-weight-bold text-center">Form Identitas Pendaki</h1>
            <div class="box">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header bg-success" id="headingOne">
                            <h5 class="mb-0">
                                <a class="btn text-decoration-none text-center font-weight-bold"
                                   data-toggle="collapse"
                                   data-target="#collapseOne" type="button" aria-expanded="true"
                                   aria-controls="collapseOne">
                                    Leader
                                </a>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                             data-parent="#accordion">
                            <div class="card-body">
                                <div class="form-group">
                                    <input name="nama_lengkap" type="text" class="form-control"
                                           placeholder="Nama Lengkap">
                                </div>
                                <div class="form-group">
                                    <input name="tanggal_lahir" type="text" class="form-control"
                                           placeholder="Tanggal Lahir">
                                </div>
                                <div class="form-group">
                                    <select class="form-control" name="jenis_kelamin">
                                        <option>Pilih Jenis Kelamin</option>
                                        <option value="l">Laki Laki</option>
                                        <option value="p">Perempuan</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input name="pekerjaan" type="text" class="form-control" placeholder="Pekerjaan">
                                </div>
                                <div class="form-group">
                                <textarea name="alamat_rumah" rows="5" cols="80" class="form-control"
                                          placeholder="Alamat Rumah"></textarea>
                                </div>
                                <div class="form-group">
                                    <select class="form-control" name="provinsi">
                                        <option>Provinsi</option>
                                        <option value="Jawa Timur">Jawa Timur</option>
                                        <option value="Jawa Tengah">Jawa Tengah</option>
                                        <option value="Jawa Barat">Jawa Barat</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="form-control" name="kabupaten">
                                        <option>Kabupaten</option>
                                        <option value="Surabaya">Surabaya</option>
                                        <option value="Semarang">Semarang</option>
                                        <option value="Bandung">Bandung</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="form-control" name="kartu_identitas">
                                        <option>Kartu Identitas</option>
                                        <option value="KTP">KTP</option>
                                        <option value="SIM">SIM</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input name="nomor_identitas" type="text" class="form-control"
                                           placeholder="Nomor Identitas">
                                </div>
                                <div class="form-group">
                                    <input name="berlaku_sampai" type="text" class="form-control"
                                           placeholder="Berlaku Sampai">
                                </div>
                                <div class="form-group">
                                    <input name="telepon_rumah" type="text" class="form-control"
                                           placeholder="Telepon Rumah ( Optional )">
                                </div>
                                <div class="form-group">
                                    <input name="handphone" type="text" class="form-control" placeholder="Handphone">
                                </div>
                                <div class="form-group">
                                    <input name="email" type="email" class="form-control" placeholder="Email">
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php for ($i = 1; $i < $inJumlah; $i++) { ?>
                        <div class="card">
                            <div class="card-header bg-success" id="<?php echo "heading" . $i; ?>">
                                <h5 class="mb-0">
                                    <a class="btn text-decoration-none text-center font-weight-bold"
                                       data-toggle="collapse" data-target="<?php echo "#collapse" . $i; ?>"
                                       aria-expanded="true" aria-controls="<?php echo "collapse" . $i; ?>">
                                        <?php echo "Pendaki " . $i; ?>
                                    </a>
                                </h5>
                            </div>
                            <div id="<?php echo "collapse" . $i; ?>" class="collapse"
                                 aria-labelledby="<?php echo "heading" . $i; ?>" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="form-group">
                                        <input name="nama_lengkap<?= $i; ?>" type="text" class="form-control"
                                               placeholder="Nama Lengkap">
                                    </div>
                                    <div class="form-group">
                                        <input name="tanggal_lahir<?= $i; ?>" type="text" class="form-control"
                                               placeholder="Tanggal Lahir">
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" name="jenis_kelamin<?= $i; ?>">
                                            <option>Pilih Jenis Kelamin</option>
                                            <option value="l">Laki Laki</option>
                                            <option value="p">Perempuan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input name="pekerjaan<?= $i; ?>" type="text" class="form-control"
                                               placeholder="Pekerjaan">
                                    </div>
                                    <div class="form-group">
                                    <textarea name="alamat_rumah<?= $i; ?>" rows="5" cols="80" class="form-control"
                                              placeholder="Alamat Rumah"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" name="provinsi<?= $i; ?>">
                                            <option>Provinsi</option>
                                            <option value="Jawa Timur">Jawa Timur</option>
                                            <option value="Jawa Tengah">Jawa Tengah</option>
                                            <option value="Jawa Barat">Jawa Barat</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" name="kabupaten<?= $i; ?>">
                                            <option>Kabupaten</option>
                                            <option value="Surabaya">Surabaya</option>
                                            <option value="Semarang">Semarang</option>
                                            <option value="Bandung">Bandung</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" name="kartu_identitas<?= $i; ?>">
                                            <option>Kartu Identitas</option>
                                            <option value="KTP">KTP</option>
                                            <option value="SIM">SIM</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input name="nomor_identitas<?= $i; ?>" type="text" class="form-control"
                                               placeholder="Nomor Identitas">
                                    </div>
                                    <div class="form-group">
                                        <input name="berlaku_sampai<?= $i; ?>" type="text" class="form-control"
                                               placeholder="Berlaku Sampai">
                                    </div>
                                    <div class="form-group">
                                        <input name="telepon_rumah<?= $i; ?>" type="text" class="form-control"
                                               placeholder="Telepon Rumah ( Optional )">
                                    </div>
                                    <div class="form-group">
                                        <input name="handphone<?= $i; ?>" type="text" class="form-control"
                                               placeholder="Handphone">
                                    </div>
                                    <div class="form-group">
                                        <input name="email<?= $i; ?>" type="email" class="form-control"
                                               placeholder="Email">
                                    </div>

                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col-10 mt-5 mb-5">
                        <input type="checkbox" name="setuju">Dengan ini saya setuju mengenai <a
                                style="color: green; font-weight: bold;" href="#">Privacy Policy</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center">
                        <button type="submit" class="btn btn-primary" name="btnPesan">Pesan Sekarang</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include "partition/footer.php" ?>

</body>
</html>
