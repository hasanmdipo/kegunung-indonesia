<?php
include __DIR__ . "/../session.php";

use session\session;

$sessionHandler = new session();

if (!$sessionHandler->isRegistered()) {
    ?>


    <nav class="navbar navbar-expand-sm">
        <a href="index.php" class="navbar-brand">
            <img src="image/logo.png" height="50" alt="logo kegunung"/>
        </a>
        <div class="navbar-nav">
            <i class="fa fa-search" aria-hidden="true"></i>
            <form class="" action="detail-gunung.php" method="post">
              <input class="form-control mr-sm-2 search-bar" type="search" placeholder="Cari Gunung Sekarang!"/>
            </form>
        </div>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a href="login.php" class="btn btn-login"><i class="fa fa-user-circle fa-lg" aria-hidden="true"></i>Login</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-daftar" href="daftar.php">Daftar</a>
            </li>
        </ul>
    </nav>

    <?php
} else {
    ?>


    <nav class="navbar navbar-expand-sm">
        <a href="index.php" class="navbar-brand">
            <img src="image/logo.png" height="50" alt="logo kegunung"/>
        </a>
        <div class="navbar-nav">
            <i class="fa fa-search" aria-hidden="true"></i>
            <form class="" action="detail-gunung.php" method="post">
              <input name="search" class="form-control mr-sm-2 search-bar" type="search" placeholder="Cari Gunung Sekarang!"/>
            </form>
        </div>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a href="history.php" class="btn btn-login">Booking</a>
            </li>
            <li class="nav-item">
                <a href="profile.php"
                   class="btn btn-login">Hi, <?= ucfirst($sessionHandler->get('current_useremail')); ?></a>
            </li>
            <li class="nav-item">
                <a class="btn btn-daftar" href="logout.php">Logout</a>
            </li>
        </ul>
    </nav>

    <?php
}
?>
