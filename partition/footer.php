<div class="row socials">
  <div class="col-12 text-center d-inline">
    <a href="https://www.facebook.com/kegunungapp" target="_blank"><img src="https://www.facebook.com/images/fb_icon_325x325.png" alt="" class="rounded" height="40px;" width="40px"></a>
    <a href="https://instagram.com/kegunungapp" target="_blank"><img src="https://instagram-brand.com/wp-content/themes/ig-branding/assets/images/ig-logo-email.png" alt="" class="rounded" height="40px;" width="40px"></a>
  </div>
</div>
<footer>
  <div class="row">
    <div class="col-xs-12 col-lg-3 text-left">
      <img src="image/logo.png" alt="kegunung-indonesia" width="80%">
      <p>Jl. Radio Kosan Hasan, Telkom University</p>
      <div class="contact">
        <p><i class="fa fa-phone"></i> +62 234 433 23</p>
        <p><i class="fa fa-envelope"></i> contact@kegunung.com</p>
      </div>
    </div>
    <div class="col-xs-12 col-lg-3">
      <h2>Halaman</h2>
      <ul>
        <li><a href="#">Kebijakan Privasi</a></li>
        <li><a href="#">Syarat dan Ketentuan</a></li>
      </ul>
    </div>
    <div class="col-xs-12 col-lg-3">
      <h2>Fitur</h2>
      <ul>
        <li>Ticketing</li>
        <li>Artikel</li>
        <li>Document</li>
      </ul>
    </div>
    <div class="col-xs-12 col-lg-3">
      <h2>Dapatkan Aplikasi</h2>
      <img src="https://play.google.com/intl/en_us/badges/static/images/badges/id_badge_web_generic.png" alt="" width="80%">
    </div>
  </div>
</footer>
<div class="text-footer row">
  <div class="col-12 text-center">
    Copyright &copy; Kegunung-Indonesia 2019
  </div>
</div>



<script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="node_modules/popper.js/dist/popper.min.js"></script>
<script type="text/javascript" src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="node_modules/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript">

</script>
