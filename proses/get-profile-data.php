<?php

include "../connection.php";
include '../partition/navbar.php';

if(!$sessionHandler->isRegistered()){
    header('Location: index.php');
    return;
}


$id = mysqli_real_escape_string($conn, $_GET['id']);

$get = $conn->prepare('SELECT * FROM user WHERE id = ?');
$get->bind_param('i', $id);
$get->execute();
$get->store_result();

if($get->num_rows == 0){
    header('Location: index.php');
    return;
}