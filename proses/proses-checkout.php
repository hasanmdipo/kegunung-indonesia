<?php

include "../partition/navbar.php";

if(!$sessionHandler->isRegistered()){
    header('Location: login.php');
    return;
}

include "../connection.php";

/*if(!isset($_POST['btnCheckout'])){
    header('index.php');
    return;
}*/


//Input Organisasi
$inGunung = $sessionHandler->get('inGunung');
$inNaik   = $sessionHandler->get('inNaik');
$inTurun  = $sessionHandler->get('inTurun');
$inJumlah = $sessionHandler->get('inJumlah');

$namaOrganisasi    = $sessionHandler->get('namaOrganisasi');
$alamatOrganisasi  = $sessionHandler->get('alamatOrganisasi');
$teleponOrganisasi = $sessionHandler->get('teleponOrganisasi');

$insertOrganisasi = $conn->prepare(
    'INSERT INTO pemesanan(id_gunung, tanggal_naik, tanggal_turun, organisasi, alamat, telfon) 
VALUES (?,?,?,?,?,?)'
);

$insertOrganisasi->bind_param(
    'issssi',
    $inGunung,
    $inNaik,
    $inTurun,
    $inJumlah,
    $namaOrganisasi,
    $alamatOrganisasi,
    $teleponOrganisasi
);

$insertOrganisasi->execute();
$insertOrganisasi->store_result();
if($insertOrganisasi->num_rows != 0){
    echo "<script>alert('Organisasi berhasil ditambbahkan');</script>";
    $sessionHandler->set('idPemesanan', $insertOrganisasi->insert_id);
}else{
    echo "<script>alert('Organisasi gagal ditambbahkan');</script>";
}


//Input Leader
$nama_lengkap    = $sessionHandler->get('nama_lengkap');
$tanggal_lahir   = $sessionHandler->get('tanggal_lahir');
$jenis_kelamin   = $sessionHandler->get('jenis_kelamin');
$pekerjaan       = $sessionHandler->get('pekerjaan');
$alamat_rumah    = $sessionHandler->get('alamat_rumah');
$provinsi        = $sessionHandler->get('provinsi');
$kabupaten       = $sessionHandler->get('kabupaten');
$kartu_identitas = $sessionHandler->get('kartu_identitas');
$nomor_identitas = $sessionHandler->get('nomor_identitas');
$berlaku_sampai  = $sessionHandler->get('berlaku_sampai');
$telepon_rumah   = $sessionHandler->get('telepon_rumah');
$handphone       = $sessionHandler->get('handphone');
$email           = $sessionHandler->get('email');

$insertLeader = $conn->prepare('INSERT INTO pendaki_info(nama_lengkap, tanggal_lahir, jenis_kelamin, pekerjaan, alamat, provinsi, kabupaten, kartu_identitas, nomor_identitas, berlaku_sampai, nomor_rumah, handphone, email) 
VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)');
$insertLeader->bind_param('ssssssssisiis',
    $sessionHandler->get('nama_lengkap'),
    $sessionHandler->get('tanggal_lahir'),
    $sessionHandler->get('jenis_kelamin'),
    $sessionHandler->get('pekerjaan'),
    $sessionHandler->get('alamat_rumah'),
    $sessionHandler->get('provinsi'),
    $sessionHandler->get('kabupaten'),
    $sessionHandler->get('kartu_identitas'),
    $sessionHandler->get('nomor_identitas'),
    $sessionHandler->get('berlaku_sampai'),
    $sessionHandler->get('telepon_rumah'),
    $sessionHandler->get('handphone'),
    $sessionHandler->get('email')
);
$insertLeader->execute();
$insertLeader->store_result();
if($insertLeader->num_rows != 0){
    echo "<script>alert('Leader berhasil ditambahkan');</script>";
    $sessionHandler->set('idLeader', $insertLeader->insert_id);
}else{
    echo "<script>alert('Leader gagal ditambahkan');</script>";
}


//Input Pendaki
for($i = 1; $i < $inJumlah; $i++){
    $insertPendakiInfo = $conn->prepare('INSERT INTO pendaki_info(nama_lengkap, tanggal_lahir, jenis_kelamin, pekerjaan, alamat, provinsi, kabupaten, kartu_identitas, nomor_identitas, berlaku_sampai, nomor_rumah, handphone, email) 
VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)');
    $insertPendakiInfo->bind_param('ssssssssisiis',
        $sessionHandler->get('nama_lengkap' . $i),
        $sessionHandler->get('tanggal_lahir' . $i),
        $sessionHandler->get('jenis_kelamin' . $i),
        $sessionHandler->get('pekerjaan' . $i),
        $sessionHandler->get('alamat_rumah' . $i),
        $sessionHandler->get('provinsi' . $i),
        $sessionHandler->get('kabupaten' . $i),
        $sessionHandler->get('kartu_identitas' . $i),
        $sessionHandler->get('nomor_identitas' . $i),
        $sessionHandler->get('berlaku_sampai' . $i),
        $sessionHandler->get('telepon_rumah' . $i),
        $sessionHandler->get('handphone' . $i),
        $sessionHandler->get('email' . $i)
    );

    $insertPendakiInfo->execute();
    $insertPendakiInfo->store_result();
    if($insertPendakiInfo->num_rows != 0){
        echo "<script>alert('Pendaki No. ". $i ." berhasil ditambahkan');</script>";
        $sessionHandler->set('idPendaki' . $i, $insertPendakiInfo->insert_id);
    }else{
        echo "<script>alert('Pendaki No. ". $i ." gagal ditambahkan');</script>";
    }
}

for($i = 1; $i < $inJumlah; $i++){
    $insertPendaki = $conn->prepare('INSERT INTO pendaki (user_id, pendaki_id, leader_id, id_pemesanan)
VALUES(?,?,?,?)');
    $insertPendaki->bind_param(
        'iiii',
        $sessionHandler->get('current_userid'),
        $sessionHandler->get('idPendaki' . $i),
        $sessionHandler->get('idLeader'),
        $sessionHandler->get('idPemesanan')
    );

    if($insertPendaki->num_rows == 0){
        echo "<script>alert('Gagal memasukkan data ke table pendaki. error no : ". $i ."');</script>";
    }else{
        echo "<script>alert('Berhasil memasukkan data ke table pendaki. success no : ". $i ."');</script>";
    }
}

$hitungHari = abs(strtotime($inNaik) - strtotime($inTurun));
$unixHari = $hitungHari / 86400;
$jumlahHari = intVal($unixHari);

$hargaGunung = $sessionHandler->get('hargaGunung');
$totalHarga = $inJumlah * ($jumlahHari * $hargaGunung);

$metode_pembayaran = mysqli_real_escape_string($conn, $_POST['method']);
$mPembayaran = "";
if($metode_pembayaran == 'mandiri'){
    $mPembayaran = "mandiri";
}else if($metode_pembayaran == 'bca'){
    $mPembayaran = 'bca';
}else if($metode_pembayaran == 'indomaret'){
    $mPembayaran = 'indomaret';
}

$insertPembayaran = $conn->prepare('INSERT INTO pembayaran (id_pemesanan, metode, total, kode_pembayaran, status) VALUES(?,?,?,?,?)');
$insertPembayaran->bind_param(
    'isisi',
    $sessionHandler->get('idPemesanan'),
    $mPembayaran,
    generateRandomString(6),
    0
);

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}