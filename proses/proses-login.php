<?php
include "../connection.php";
include '../partition/navbar.php';

if(isset($_POST['login'])){
    $isEmail = false;
    $useremail = mysqli_real_escape_string($conn, $_POST['inUsrEmail']);
    $password  = mysqli_real_escape_string($conn, $_POST['inPassword']);

    if(empty($useremail) || empty($password)){
        ?>
        <script>
            alert('Email dan password harus diisi');
            window.location.replace('../');
        </script>
        <?php
    }

    if(filter_var($useremail, FILTER_VALIDATE_EMAIL)){
        $isEmail = true;
    }

    if($isEmail){
        $login = $conn->prepare('SELECT id FROM user WHERE email = ? AND password = ?');
        $login->bind_param('ss', $useremail, md5($password));
        $login->execute();
        $login->store_result();

        $login->bind_result($userid);

        if($login->num_rows != 0){
            $sessionHandler->register();
            $sessionHandler->set('current_useremail', $useremail);

            //get data and entry session
            $get_data = mysqli_query($conn, "SELECT * FROM user WHERE email = '$useremail'");
            $data_array = mysqli_fetch_assoc($get_data);
            $sessionHandler->set('current_username', $data_array['username']);
            $sessionHandler->set('current_firstname', $data_array['nama_depan']);
            $sessionHandler->set('current_userid', $data_array['id']);

            ?>
            <script>
                alert('Login berhasil');
                window.location.replace('../');
            </script>
            <?php
        }else{
            ?>
            <script>
                alert('Email/password salah');
                window.location.replace('../login.php');
            </script>
            <?php
        }
    }else{
        $login = $conn->prepare('SELECT * FROM user WHERE username = ? AND password = ?');
        $login->bind_param('ss', $useremail, md5($password));
        $login->execute();
        $login->store_result();

        if($login->num_rows != 0){
            $sessionHandler->register();
            $sessionHandler->set('current_username', $useremail);

            $get_data = mysqli_query($conn, "SELECT * FROM user WHERE username = '$useremail'");
            $data_array = mysqli_fetch_assoc($get_data);
            $sessionHandler->set('current_useremail', $data_array['email']);
            $sessionHandler->set('current_firstname', $data_array['nama_depan']);
            $sessionHandler->set('current_userid', $data_array['id']);

            ?>
            <script>
                alert('Login berhasil');
                window.location.replace('../');
            </script>
            <?php

        }else{
            ?>
            <script>
                alert('Username/password salah');
                window.location.replace('../login.php');
            </script>
            <?php

        }
    }
}
