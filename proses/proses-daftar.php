<?php

include "../connection.php";
include "../partition/navbar.php";

if(!$sessionHandler->isRegistered()){
    header('Location: login.php');
    return;
}
if (isset($_POST['daftar'])) {
    $username = mysqli_real_escape_string($conn, $_POST['usr']);
    $nama_depan = mysqli_real_escape_string($conn, $_POST['namaD']);
    $nama_belakang = mysqli_real_escape_string($conn, $_POST['namaB']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $password = mysqli_real_escape_string($conn, $_POST['pwd']);
    $repassword = mysqli_real_escape_string($conn, $_POST['repwd']);

    if ($password != $repassword) {
        echo "<script>
    alert('password anda tidak sama !');
    window.location.href = 'daftar.php';
    </script>";
    }

    $checkuser = $conn->prepare('SELECT * FROM user WHERE username = ?');
    $checkuser->bind_param('s', $username);
    $checkuser->execute();
    $checkuser->store_result();

    if($checkuser->num_rows != 0){
        ?>
        <script>
            alert('Username telah terdaftar!');
            window.location.href = '../daftar.php';
        </script>
        <?php
        return;
    }

    $checkemail = $conn->prepare('SELECT * FROM user WHERE email = ?');
    $checkemail->bind_param('s', $email);
    $checkemail->execute();
    $checkemail->store_result();

    if($checkemail->num_rows != 0){
        ?>
        <script>
            alert('Email telah terdaftar!');
            window.location.href = '../daftar.php';
        </script>
        <?php
        return;
    }

    $password = md5($password);

    $daftar = $conn->prepare('INSERT INTO user (username, nama_depan, nama_belakang, email, password) VALUES(?, ?, ?, ?, ?)');
    $daftar->bind_param('sssss', $username, $nama_depan, $nama_belakang, $email, $password);
    $daftar->execute();
    $daftar->store_result();

    if ($daftar->affected_rows == 0) {
        ?>
        <script>
            alert('Gagal Daftar!');
            window.location.href = '../daftar.php';
        </script>
        <?php
        return;
    }

    ?>
    <script>
        alert('Berhasil Daftar!');
        window.location.href = '../login.php';
    </script>
    <?php

}


?>
