<?php

include "../partition/navbar.php";

if(!$sessionHandler->isRegistered()){
    header('Location: login.php');
    return;
}

if(!isset($_POST['btnPesan'])){
    header('Location: index.php');
    return;
}

$inJumlah = $sessionHandler->get('inJumlah');

if(empty($inJumlah) || $inJumlah === null){
    header('Location: index.php');
    return;
}

include "../connection.php";

//unset session
$sessionHandler->unSession('namaOrganisasi');
$sessionHandler->unSession('alamatOrganisasi');
$sessionHandler->unSession('teleponOrganisasi');

$sessionHandler->unSession('nama_lengkap');
$sessionHandler->unSession('tanggal_lahir');
$sessionHandler->unSession('jenis_kelamin');
$sessionHandler->unSession('pekerjaan');
$sessionHandler->unSession('alamat_rumah');
$sessionHandler->unSession('provinsi');
$sessionHandler->unSession('kabupaten');
$sessionHandler->unSession('kartu_identitas');
$sessionHandler->unSession('nomor_identitas');
$sessionHandler->unSession('berlaku_sampai');
$sessionHandler->unSession('telepon_rumah');
$sessionHandler->unSession('handphone');
$sessionHandler->unSession('email');

for($i = 1; $i < $inJumlah; $i++){
    $sessionHandler->unSession('nama_lengkap' . $i);
    $sessionHandler->unSession('tanggal_lahir' . $i);
    $sessionHandler->unSession('jenis_kelamin' . $i);
    $sessionHandler->unSession('pekerjaan' . $i);
    $sessionHandler->unSession('alamat_rumah' . $i);
    $sessionHandler->unSession('provinsi' . $i);
    $sessionHandler->unSession('kabupaten' . $i);
    $sessionHandler->unSession('kartu_identitas' . $i);
    $sessionHandler->unSession('nomor_identitas' . $i);
    $sessionHandler->unSession('berlaku_sampai' . $i);
    $sessionHandler->unSession('telepon_rumah' . $i);
    $sessionHandler->unSession('handphone' . $i);
    $sessionHandler->unSession('email' . $i);
}

//POST Organisasi
$namaOrganisasi    = $_POST['namaOrganisasi'];
$alamatOrganisasi  = $_POST['alamatOrganisasi'];
$teleponOrganisasi = $_POST['teleponOrganisasi'];

//Info Organisasi
$sessionHandler->set('namaOrganisasi', $namaOrganisasi);
$sessionHandler->set('alamatOrganisasi', $alamatOrganisasi);
$sessionHandler->set('teleponOrganisasi', $teleponOrganisasi);

//Info Leader
$sessionHandler->set('nama_lengkap', mysqli_real_escape_string($conn, $_POST['nama_lengkap']));
$sessionHandler->set('tanggal_lahir', mysqli_real_escape_string($conn, $_POST['tanggal_lahir']));
$sessionHandler->set('jenis_kelamin', mysqli_real_escape_string($conn, $_POST['jenis_kelamin']));
$sessionHandler->set('pekerjaan', mysqli_real_escape_string($conn, $_POST['pekerjaan']));
$sessionHandler->set('alamat_rumah', mysqli_real_escape_string($conn, $_POST['alamat_rumah']));
$sessionHandler->set('provinsi', mysqli_real_escape_string($conn, $_POST['provinsi']));
$sessionHandler->set('kabupaten', mysqli_real_escape_string($conn, $_POST['kabupaten']));
$sessionHandler->set('kartu_identitas', mysqli_real_escape_string($conn, $_POST['kartu_identitas']));
$sessionHandler->set('nomor_identitas', mysqli_real_escape_string($conn, $_POST['nomor_identitas']));
$sessionHandler->set('berlaku_sampai', mysqli_real_escape_string($conn, $_POST['berlaku_sampai']));
$sessionHandler->set('telepon_rumah', mysqli_real_escape_string($conn, $_POST['telepon_rumah']));
$sessionHandler->set('handphone', mysqli_real_escape_string($conn, $_POST['handphone']));
$sessionHandler->set('email', mysqli_real_escape_string($conn, $_POST['email']));


//Info Pendaki 1 - n
for ($i = 1; $i < $inJumlah; $i++){
    $sessionHandler->set('nama_lengkap' . $i, mysqli_real_escape_string($conn, $_POST['nama_lengkap' . $i]));
    $sessionHandler->set('tanggal_lahir' . $i, mysqli_real_escape_string($conn, $_POST['tanggal_lahir' . $i]));
    $sessionHandler->set('jenis_kelamin' . $i, mysqli_real_escape_string($conn, $_POST['jenis_kelamin' . $i]));
    $sessionHandler->set('pekerjaan' . $i, mysqli_real_escape_string($conn, $_POST['pekerjaan' . $i]));
    $sessionHandler->set('alamat_rumah' . $i, mysqli_real_escape_string($conn, $_POST['alamat_rumah' . $i]));
    $sessionHandler->set('provinsi' . $i, mysqli_real_escape_string($conn, $_POST['provinsi' . $i]));
    $sessionHandler->set('kabupaten' . $i, mysqli_real_escape_string($conn, $_POST['kabupaten' . $i]));
    $sessionHandler->set('kartu_identitas' . $i, mysqli_real_escape_string($conn, $_POST['kartu_identitas' . $i]));
    $sessionHandler->set('nomor_identitas' . $i, mysqli_real_escape_string($conn, $_POST['nomor_identitas' . $i]));
    $sessionHandler->set('berlaku_sampai' . $i, mysqli_real_escape_string($conn, $_POST['berlaku_sampai' . $i]));
    $sessionHandler->set('telepon_rumah' . $i, mysqli_real_escape_string($conn, $_POST['telepon_rumah' . $i]));
    $sessionHandler->set('handphone' . $i, mysqli_real_escape_string($conn, $_POST['handphone' . $i]));
    $sessionHandler->set('email' . $i, mysqli_real_escape_string($conn, $_POST['email' . $i]));
}

header('Location: /checkout.php');
return;