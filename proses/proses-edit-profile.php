<?php
include "../partition/navbar.php";
include "../connection.php";

if(!$sessionHandler->isRegistered()){
    header('Location: /profile.php');
    return;
}

$idUser     = $sessionHandler->get('current_userid');
$email      = mysqli_real_escape_string($conn, $_POST['inEmail']);
$nama_depan = mysqli_real_escape_string($conn, $_POST['inNamaDepan']);
$nama_belakang = mysqli_real_escape_string($conn, $_POST['inNamaBelakang']);
$tanggal_lahir = mysqli_real_escape_string($conn, $_POST['inTanggalLahir']);
$jenis_kelamin = mysqli_real_escape_string($conn, $_POST['inJenisKelamin']);
$pekerjaan  = mysqli_real_escape_string($conn, $_POST['inPekerjaan']);
$alamat     = mysqli_real_escape_string($conn, $_POST['inAlamat']);
$provinsi   = mysqli_real_escape_string($conn, $_POST['inProvinsi']);
$kabupaten  = mysqli_real_escape_string($conn, $_POST['inKabupaten']);
$nomor_identitas = mysqli_real_escape_string($conn, $_POST['inNomorIdentitas']);
$berlaku_sampai  = mysqli_real_escape_string($conn, $_POST['inBerlakuSampai']);
$telepon_rumah   = mysqli_real_escape_string($conn, $_POST['inTelepon']);
$handphone       = mysqli_real_escape_string($conn, $_POST['inHandphone']);

$update = $conn->prepare("UPDATE user SET email = ?, nama_depan = ?, nama_belakang = ?, tanggal_lahir = ?,
jenis_kelamin = ?, pekerjaan = ?, alamat = ?, provinsi = ?, kabupaten = ?, nomor_identitas = ?, berlaku_sampai = ?,
telepon_rumah = ?, handphone = ? WHERE id = ?");

$update->bind_param('sssssssssisiii', $email, $nama_depan, $nama_belakang, $tanggal_lahir, $jenis_kelamin, $pekerjaan,
    $alamat, $provinsi, $kabupaten, $nomor_identitas, $berlaku_sampai, $telepon_rumah, $handphone, $idUser);
$update->execute();
$update->store_result();

if($update->affected_rows != 0){
    ?>
    <script>
        alert('Berhasil mengupdate profile');
        window.location.href = '../profile.php';
    </script>
    <?php
}else{
    echo "
    <script>
        alert('Gagal mengupdate profile');
        window.location.href = 'profile.php';
    </script>";
}
