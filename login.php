<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Kegunung Indonesia</title>
  <?php include"partition/header.php" ?>
  <link rel="stylesheet" href="css/login.css">
</head>
<body>

  <?php include"partition/navbar.php" ?>

  <?php
  if($sessionHandler->isRegistered()){
      header('Location: /');
      return;
  }
  ?>

<form action="proses/proses-login.php" method="post" class="text-center form-login">
	 <h1>Login</h1>
	<div class="form-group">
		<div class="col-xs-12 col-lg-12">
			<input type="text" class="form-control" id="usr" name="inUsrEmail" placeholder="Username/Email">
		</div>
	</div>
	<div class="form-group">
		<div class="col-xs-12 col-lg-12">
			<input type="password" class="form-control" id="pwd" name="inPassword" placeholder="Password">
		</div>
	</div>
	<div class="row">
		<div class="col-4">
			<input type="checkbox" name="ingatsaya">Inget saya
		</div>
		<div class="col-4"></div>

		<div class="col-4 ">
			<a class="lupapw" href="#lupapassword">Lupa Password</a>
		</div>
	</div><br>
	<button class="btn btn-success button" type="submit" name="login">Login</button>
	<p>Buat Akun?
	<a href="#daftar" class="daftar">Daftar</a></p>
</form>


  <?php include"partition/footer.php" ?>

</body>
</html>
