<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Kegunung Indonesia - Profile</title>
  <?php include "partition/header.php" ?>
  <link rel="stylesheet" href="css/profile.css">
</head>
<body>

  <?php include "partition/navbar.php" ?>
  <?php

  include "connection.php";

  $useremail = $sessionHandler->get('current_useremail');
  $profile = $conn->prepare('SELECT username, email, nama_depan, nama_belakang, tanggal_lahir, jenis_kelamin,
    pekerjaan, alamat, provinsi, kabupaten, nomor_identitas, berlaku_sampai, telepon_rumah, handphone FROM user WHERE username = ? OR email = ?');
    $profile->bind_param('ss', $useremail, $useremail);
    $profile->execute();
    $profile->store_result();

    if ($profile->num_rows == 0) {
      ?>
      <script>
      alert('Something went wrong');
      window.location.replace('profile.php');
      </script>
      <?php
      return;
    }

    $profile->bind_result($user, $email, $nama_depan, $nama_belakang, $tanggal_lahir, $jenis_kelamin,
    $pekerjaan, $alamat, $provinsi, $kabupaten, $nomor_identitas, $berlaku_sampai, $telepon_rumah, $handphone);


    while ($profile->fetch()) {
      ?>

      <div class="row mb-5">
        <div class="col-12">
          <h1>Edit Profile</h1>
        </div>
      </div>
      <form method="post" action="proses/proses-edit-profile.php">
        <div class="row" style="margin:0px 15%">
          <div class="col-xs-12 col-lg-4">
            <div class="row form-group">
              <div class="col-xs-12 col-lg-12">
                <div class="text-center">
                  <img src="//placehold.it/150" class="avatar img-circle" alt="avatar">
                  <h6 class="userpf"><?= $user ?></h6>
                </div>
              </div>
            </div>
            <div class="row form-group form-profile">
              <div class="col-xs-12 col-lg-12 text-left">
                <label for="inEmail">Email</label>
                <input type="email" class="form-control" id="inEmail" name="inEmail" value="<?= $email ?>">
              </div>
            </div>
            <div class="row form-group form-profile">
              <div class="col-xs-12 col-lg-12 text-left">
                <label for="inNamaDepan">Nama Depan</label>
                <input type="text" class="form-control" id="inNamaDepan" name="inNamaDepan"
                value="<?= $nama_depan ?>">
              </div>
            </div>
            <div class="row form-group form-profile">
              <div class="col-xs-12 col-lg-12 text-left">
                <label for="inNamaBelakang">Nama Belakang</label>
                <input type="text" class="form-control" id="inNamaBelakang" name="inNamaBelakang"
                value="<?= $nama_belakang ?>">
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-lg-4">
            <div class="form-group">
              <div class="col-xs-12 col-lg-12">
                <label for="inTanggalLahir">Tanggal Lahir</label>
                <input type="text" class="form-control" id="inTanggalLahir" name="inTanggalLahir"
                placeholder="dd/mm/yyyy" value="<?= $jenis_kelamin ?>">
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-12 col-lg-12">
                <label for="inJenisKelamin">Jenis Kelamin</label>
                <select class="form-control select2" id="inJenisKelamin" name="inJenisKelamin">
                  <?php
                  if ($jenis_kelamin === 'P') {
                    ?>
                    <option value="P" selected>Pria</option>
                    <option value="W">Wanita</option>
                    <?php
                  } else if ($jenis_kelamin === 'W') {
                    ?>
                    <option value="P">Pria</option>
                    <option value="W" selected>Wanita</option>
                    <?php
                  } else {
                    ?>
                    <option value="P">Pria</option>
                    <option value="W">Wanita</option>
                    <?php
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-12 col-lg-12">
                <label for="inPekerjaan">Pekerjaan</label>
                <input type="text" class="form-control" id="inPekerjaan" name="inPekerjaan"
                placeholder="Anda bekerja sebagai apa?" value="<?= $pekerjaan ?>">
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-12 col-lg-12">
                <label for="inAlamat">Alamat Rumah</label>
                <textarea name="inAlamat" id="inAlamat" rows="4" cols="80"
                class="form-control"><?= $alamat ?></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-12 col-lg-12">
                <label for="inProvinsi">Provinsi</label>
                <input type="text" class="form-control" id="inProvinsi" name="inProvinsi"
                placeholder="Provinsi" value="<?= $provinsi ?>">
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-12 col-lg-12">
                <label for="inKabupaten">Kabupaten</label>
                <input type="text" class="form-control" id="inKabupaten" name="inKabupaten"
                placeholder="Kabupaten" value="<?= $kabupaten ?>">
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-lg-4">
            <div class="form-group">
              <div class="col-xs-12 col-lg-12">
                <label for="inNomorIdentitas">Nomor Identitas</label>
                <input type="text" class="form-control" id="inNomorIdentitas" name="inNomorIdentitas"
                placeholder="32478xxxxxxx" value="<?= $nomor_identitas ?>">
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-12 col-lg-12">
                <label for="inBerlakuSampai">Berlaku Sampai</label>
                <input type="text" class="form-control" id="inBerlakuSampai" name="inBerlakuSampai"
                placeholder="mm/yy">
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-12 col-lg-12">
                <label for="inTelepon">Telepon Rumah</label>
                <input type="text" class="form-control" id="inTelepon" name="inTelepon"
                placeholder="021 888 8888" value="<?= $telepon_rumah ?>">
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-12 col-lg-12">
                <label for="inHandphone">Handphone</label>
                <input type="text" class="form-control" id="inHandphone" name="inHandphone"
                placeholder="+62 888 8888" value="<?= $handphone ?>">
              </div>
            </div>
          </div>
        </div>
        <div class="row mb-5">
          <div class="col-12">
            <div class="su">
              <button type="submit" class="btn btn-info">Edit Profile</button>
            </div>
          </div>
        </div>
      </form>

      <?php
    }
    ?>
  </div>

  <?php include "partition/footer.php" ?>

</body>
</html>
