<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Kegunung Indonesia - Detail Gunung</title>
  <?php include "partition/header.php" ?>
  <link rel="stylesheet" href="css/detail.css">
</head>
<body>

  <?php
  include "partition/navbar.php";
  include "connection.php";
  ?>

  <?php

  if (!isset($_POST['submit']) && !isset($_POST['search'])) {
    header('Location: index.php');
    return;
  }

  if (isset($_POST['search'])) {
    include "proses/proses-search.php";
  }else{
    $idGunung = $_POST['gunung'];
    $naik = $_POST['naik'];
    $turun = $_POST['turun'];
    $jumlah = $_POST['jumlah'];
  }

  $get = $conn->prepare('SELECT nama_gunung, deskripsi, foto, alamat,
    harga_tiket, kuota, level, maps FROM gunung WHERE id = ?');
    $get->bind_param('i', $idGunung);
    $get->execute();
    $get->store_result();

    if ($get->num_rows == 0) {
      header('Location: index.php');
      return;
    }

    $get->bind_result($nama_gunung, $deskripsi, $foto, $alamat_gunung, $harga_tiket, $kuota, $level, $maps);

    while ($get->fetch()) {
      ?>

      <div class="jumbotron jumbotron-fluid text-left">
        <div class="w-50 mx-5">
          <h1 class="display-4 font-weight-bold">Naik Gunung Kini Lebih Mudah !</h1>
          <p class="lead text-left">Tidak perlu khawatir dengan rute dan iklim, pilih guider mu untuk membantu naik
            gunung
            sekarang juga</p>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-md-4">
            <div class="box">
              <form class="text-left form-daftar" method="post" action="detail-gunung.php">
                <div class="row form-group">
                  <div class="col-xs-12 col-lg-12">
                    <label for="pilih">Pilih Gunung</label>
                    <select id="pilih" class="form-control" name="gunung">
                      <option selected> --Pilih Gunung--</option>
                      <?php
                      include 'connection.php';
                      $sql = mysqli_query($conn, "SELECT * FROM gunung ORDER BY nama_gunung ASC");
                      if (mysqli_num_rows($sql) != 0) {
                        while ($row = mysqli_fetch_assoc($sql)) {
                          if ($idGunung == $row['id']) {
                            echo '<option selected value="' . $row["id"] . '">' . $row['nama_gunung'] . '</option>';
                          } else {
                            echo '<option value="' . $row["id"] . '">' . $row['nama_gunung'] . '</option>';
                          }
                        }
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-xs-12 col-lg-6">
                    <label for="naik">Naik</label>
                    <input class="form-control" type="date" id="naik" name="naik" value="<?= $naik; ?>">
                  </div>
                  <div class="col-xs-12 col-lg-6">
                    <label for="turun">Turun</label>
                    <input class="form-control" type="date" id="turun" name="turun" value="<?= $turun; ?>">
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-xs-12 col-lg-12">
                    <label for="jumlah">Jumlah Pendaki</label>
                    <select id="jumlah" class="form-control" name="jumlah">
                      <?php
                      if ($jumlah == 1) {
                        ?>
                        <option selected value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <?php
                      } else if ($jumlah == 2) {
                        ?>
                        <option value="1">1</option>
                        <option selected value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <?php
                      } else if ($jumlah == 3) {
                        ?>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option selected value="3">3</option>
                        <option value="4">4</option>
                        <?php
                      } else if ($jumlah == 4) {
                        ?>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option selected value="4">4</option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="row form-group text-center">
                  <div class="col-12">
                    <button type="submit" class="btn btn-daftar" name="submit">Cek Kuota</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="col-xs-12 col-md-8 px-5">
            <h1 class="font-weight-bold text-center"><?= $nama_gunung; ?></h1>
            <div class="row my-5">
              <div class="col-4 text-center">
                <p>Level :</p>
                <p class="font-weight-bold"><?= $level; ?></p>
              </div>
              <div class="col-4 text-center">
                <p>Sisa Kuota : <span class="font-weight-bold"><?= $kuota; ?></span></p>
                <form class="text-left form-daftar" method="post" action="reservasi.php">
                  <select id="pilih" class="form-control" name="gunung" hidden>
                    <option selected> --Pilih Gunung--</option>
                    <?php
                    include 'connection.php';
                    $sql = mysqli_query($conn, "SELECT * FROM gunung ORDER BY nama_gunung ASC");
                    if (mysqli_num_rows($sql) != 0) {
                      while ($row = mysqli_fetch_assoc($sql)) {
                        if ($idGunung == $row['id']) {
                          echo '<option selected value="' . $row["id"] . '">' . $row['nama_gunung'] . '</option>';
                        } else {
                          echo '<option value="' . $row["id"] . '">' . $row['nama_gunung'] . '</option>';
                        }
                      }
                    }
                    ?>
                  </select>
                  <input class="form-control" type="date" id="naik" name="naik" value="<?= $naik; ?>" hidden>
                  <input class="form-control" type="date" id="turun" name="turun" value="<?= $turun; ?>" hidden>
                  <select id="jumlah" class="form-control" name="jumlah" hidden>
                    <?php
                    if ($jumlah == 1) {
                      ?>
                      <option selected value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <?php
                    } else if ($jumlah == 2) {
                      ?>
                      <option value="1">1</option>
                      <option selected value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <?php
                    } else if ($jumlah == 3) {
                      ?>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option selected value="3">3</option>
                      <option value="4">4</option>
                      <?php
                    } else if ($jumlah == 4) {
                      ?>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option selected value="4">4</option>
                      <?php
                    }
                    ?>
                  </select>
                  <div class="row form-group text-center">
                    <div class="col-12">
                      <button type="submit" class="btn btn-daftar" name="btnCheckout">Pesan</button>
                    </div>
                  </div>
                </form>
              </div>
              <div class="col-4 text-center">
                <p>Ticket :</p>
                <p><span class="font-weight-bold"><?= $harga_tiket; ?></span>/hari</p>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel"
                style="width:100%;">
                <div class="carousel-inner" style="height:400px;">
                  <div class="carousel-item active">
                    <img src="image/Gunung 1.jpg" class="d-block w-100" alt="Gunung 1" width="100%"
                    height="550">
                  </div>
                  <div class="carousel-item">
                    <img src="image/Gunung 2.jpg" class="d-block w-100" alt="Gunung 2" width="100%"
                    height="550">
                  </div>
                  <div class="carousel-item">
                    <img src="image/Gunung 3.jpg" class="d-block w-100" alt="Gunung 3" width="100%"
                    height="550">
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12 my-5">
              <p><?= $deskripsi; ?></p>
            </div>
          </div>
          <div class="row">
            <div class="col-12 mb-5">
              <iframe width="100%" height="400" id="gmap_canvas"
              src="https://maps.google.com/maps?q=<?= $nama_gunung; ?>&t=&z=17&ie=UTF8&iwloc=&output=embed"
              frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
            </div>
          </div>
        </div>
      </div>


      <?php
    }
    include "partition/footer.php" ?>

  </body>
  </html>
