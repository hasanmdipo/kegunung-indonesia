<?php

include"partition/navbar.php";

if($sessionHandler->isRegistered()){
    $sessionHandler->end();
    header('Location: /');
    return;
}

header('Location: /');
?>
