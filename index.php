<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Kegunung Indonesia</title>
    <?php include "partition/header.php" ?>
    <link rel="stylesheet" href="css/index.css">
</head>
<body>

<?php include "partition/navbar.php" ?>

<div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
    <div style="position: absolute; z-index: 2; margin: auto; left: 0; bottom: 0; right: 0; height: 300px;">
        <h3 align="center" style="color: white; font-weight: bold;">Kegunung Kini Lebih Mudah!</h3>
        <p></p>
        <h6 align="center" style="color: white;">Lengkapi Dokumen Untuk Pengalaman yang Lebih Aman</h6> <br>
        <p align="center">
            <a align="center" class="btn btn-daftar" href="#">Pelajari</a>
        </p>
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="image/Gunung 1.jpg" class="d-block w-100" alt="Gunung 1" width="100%" height="550">
        </div>
        <div class="carousel-item">
            <img src="image/Gunung 2.jpg" class="d-block w-100" alt="Gunung 2" width="100%" height="550">
        </div>
        <div class="carousel-item">
            <img src="image/Gunung 3.jpg" class="d-block w-100" alt="Gunung 3" width="100%" height="550">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<div class="text-center my-5">
    <h4>Ayo #Kegunung Sekarang</h4>
</div>
<div class="box">
    <form class="text-left form-daftar" method="post" action="detail-gunung.php">
        <div class="row form-group">
            <div class="col-xs-12 col-lg-12">
                <label for="pilih">Pilih Gunung</label>
                <select id="pilih" name="gunung" class="form-control">
                    <option selected> --Pilih Gunung--</option>
                    <?php
                    include 'connection.php';
                    $sql = mysqli_query($conn, "SELECT * FROM gunung ORDER BY nama_gunung ASC");
                    if (mysqli_num_rows($sql) != 0) {
                        while ($row = mysqli_fetch_assoc($sql)) {
                            echo '<option value='. $row["id"] .'>' . $row['nama_gunung'] . '</option>';
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xs-12 col-lg-6">
                <label for="naik">Naik</label>
                <input class="form-control" type="date" id="naik" name="naik">
            </div>
            <div class="col-xs-12 col-lg-6">
                <label for="turun">Turun</label>
                <input class="form-control" type="date" id="turun" name="turun">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xs-12 col-lg-12">
                <label for="jumlah">Jumlah Pendaki</label>
                <select id="jumlah" class="form-control" name="jumlah">
                    <option selected value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
            </div>
        </div>
        <div class="row form-group text-center">
            <div class="col-12">
                <button type="submit" class="btn btn-daftar" name="submit" >Cek Kuota</button>
            </div>
        </div>
    </form>
</div>


<?php include "partition/footer.php" ?>

</body>
</html>
