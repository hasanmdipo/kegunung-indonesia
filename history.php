<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Kegunung Indonesia - Riwayat Booking</title>
  <?php include"partition/header.php" ?>
  <link rel="stylesheet" href="css/history.css">
</head>
<body>

  <?php include"partition/navbar.php" ?>

  <?php

  use session\session;

  $sessionHandler = new session();

  if (!$sessionHandler->isRegistered()) {
    header('Location: login.php');
    return;
  }

  $id = $sessionHandler->get("current_userid");

  ?>

  <?php include 'connection.php';
  $query = "SELECT pemesanan.id,gunung.nama_gunung, pemesanan.tanggal_naik
  ,pemesanan.tanggal_turun, COUNT(*) as jml_orang
  FROM pemesanan
  INNER JOIN gunung ON pemesanan.id_gunung = gunung.id
  JOIN pendaki ON pemesanan.id = pendaki.id_pemesanan
  JOIN user ON user.id = pendaki.user_id
  WHERE user.id = '$id' GROUP BY pendaki.id_pemesanan";
  $sql = mysqli_query($conn, $query);

  ?>

  <div class="row">
    <div class="col-12">
      <h1 class="font-weight-bold text-center my-5">Riwayat Booking</h1>
    </div>
    <div class="col-12 text-center">
      <img src="image/user.jpg" alt="" height="150px" width="150px" class="rounded-circle">
    </div>
    <div class="col-12">
      <h3 class="font-weight-bold text-center my-5"><?php $sessionHandler->get('current_username')  ?></h3>
    </div>
  </div>
  <div class="row">
    <?php while($gn = mysqli_fetch_assoc($sql)){ ?>
      <div class="col-12 card-history">
        <a href="booking.php?id=<?php echo $gn['id']; ?>" style="text-decoration:none; color:black;">
          <div class="card mb-4" style="max-width: 540px;">
            <div class="row no-gutters">
              <div class="col-md-4 bg-success">
                <h4>Booked</h4>
              </div>
              <div class="col-md-8">
                <div class="card-body">
                  <h5 class="card-title font-weight-bold"><?php echo $gn['nama_gunung']?></h5>
                  <p class="card-text"><?php echo $gn['jml_orang']; ?> Orang</p>
                  <p class="card-text"><?php echo $gn['tanggal_naik'] ?> - <?php echo $gn['tanggal_turun'] ?></p>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
    <?php } ?>
  </div>


  <?php include"partition/footer.php" ?>

</body>
</html>
