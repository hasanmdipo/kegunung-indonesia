<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Kegunung Indonesia - Riwayat Booking</title>
	<?php include"partition/header.php" ?>
	<link rel="stylesheet" href="css/riwayat-booking.css">
</head>
<body>

	<?php include"partition/navbar.php"?>

	<?php
	
	?>

	<h1>Riwayat Booking</h1>
	<div class="row form-group">
		<div class="col-xs-12 col-lg-12">
			<div class="text-center">
				<img src="//placehold.it/150" class="avatar img-circle" alt="avatar" style="">
				<h6>Username</h6>
			</div>
		</div>
	</div>
	<form class="gunung">
		<div class="card mb-3" style="max-width: 540px;">
			<div class="row no-gutters">
				<div class="col-md-4">
					<div class="text-center">
						<img src="image/kegunungicon.png" class="card-img" alt="gn">
					</div>
				</div>
				<div class="col-md-8">
					<div class="text-center">
						<div class="card-body">
							<h3 class="card-title">Gunung Ijen</h3>
							<p class="card-text">4 Orang</p>
							<p class="card-text"><small class="text-muted">25 Mei 2019 - 30 Mei 2019</small></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>

	<?php include"partition/footer.php"?>

</body>
</html>
