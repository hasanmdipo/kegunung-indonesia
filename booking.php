<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Kegunung Indonesia - Booking</title>
  <?php include"partition/header.php" ?>
  <link rel="stylesheet" href="css/booking.css">
</head>
<body>

  <?php include"partition/navbar.php" ?>

  <?php

  use session\session;

  $sessionHandler = new session();

  if (!$sessionHandler->isRegistered()) {
    header('Location: login.php');
    return;
  }

  $id = $sessionHandler->get("current_userid");

  include 'connection.php';

  if (!isset($_GET['id'])) {
    header('Location: index.php');
    return;
  }

  $id_pemesanan = $_GET['id'];

  $query = "SELECT pemesanan.id,gunung.nama_gunung, pemesanan.kode_booking
  FROM pemesanan
  INNER JOIN gunung ON pemesanan.id_gunung = gunung.id
  JOIN pendaki ON pemesanan.id = pendaki.id_pemesanan
  JOIN user ON user.id = pendaki.user_id
  WHERE user.id = '$id' AND pemesanan.id = '$id_pemesanan'";
  $sql = mysqli_query($conn, $query);

  $get = mysqli_fetch_assoc($sql);
  if ($get == null) {
    header('Location: index.php');
    return;
  }
  ?>


  <div class="row">
    <div class="col-12">
      <h1 class="font-weight-bold text-center my-5"><?php echo $get['nama_gunung'] ?></h1>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <!-- wizard -->
    </div>
  </div>
  <div class="row booking">
    <div class="col-12">
      <h1 class="font-weight-bold text-center">Kode Booking</h1>
    </div>
    <div class="col-12">
      <p class="text-center">Bawa kode booking berikut ke petugas gunung ijen untuk verifikasi onsite</p>
    </div>
    <div class="col-12">
      <h1 class="font-weight-bold text-center my-5"><?php echo $get['kode_booking'] ?></h1>
    </div>
    <div class="col-12 text-center mb-5">
      <img src="https://2d6qxj3uqdaw38d6lk27l0ao-wpengine.netdna-ssl.com/wp-content/uploads/2015/10/apb-qr-code.png" alt="" height="300px" width="300px">
    </div>
  </div>


  <?php include"partition/footer.php" ?>

</body>
</html>
