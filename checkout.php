<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Kegunung Indonesia - Checkout</title>
    <?php include "partition/header.php" ?>
    <link rel="stylesheet" href="css/checkout.css">
</head>
<body>

<?php include "partition/navbar.php" ?>

<?php
if (!$sessionHandler->isRegistered()) {
    header('Location: login.php');
    return;
}

//if(empty($sessionHandler->get('nama_lengkap'))){
//    header('Location: index.php');
//    return;
//}

include "connection.php";

$inGunung = $sessionHandler->get('inGunung');
$inNaik   = $sessionHandler->get('inNaik');
$inTurun  = $sessionHandler->get('inTurun');
$inJumlah = $sessionHandler->get('inJumlah');

$getGunung = $conn->prepare('SELECT nama_gunung, harga_tiket FROM gunung WHERE id = ?');
$getGunung->bind_param('i', $inGunung);
$getGunung->execute();
$getGunung->store_result();
$getGunung->bind_result($nama, $harga);

$namaGunung = '';
$hargaGunung = 0;

while ($getGunung->fetch()) {
    $namaGunung  = $nama;
    $hargaGunung = $harga;
    $sessionHandler->set('hargaGunung', $hargaGunung);
}
?>

<form action="" method="post">
    <div class="row px-5">
        <div class="col-12">
            <h1 class="font-weight-bold text-center my-5">Checkout</h1>
        </div>
        <div class="col-12">
            <h2 class="font-weight-bold table-title mb-2">Info Pemesanan</h2>
            <table class="table text-center mb-5">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Tujuan</th>
                    <th scope="col">Datang</th>
                    <th scope="col">Pulang</th>
                    <th scope="col">Harga</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row"><?= $namaGunung; ?></th>
                    <td><?= $inNaik; ?></td>
                    <td><?= $inTurun; ?></td>
                    <td>Rp. <?= $hargaGunung; ?>/Hari
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12">
            <h2 class="font-weight-bold table-title mb-2">Info Harga</h2>
            <table class="table text-center mb-5">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Tujuan</th>
                    <th scope="col">Lama</th>
                    <th scope="col">Pendaki</th>
                    <th scope="col">Total</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th><?= $namaGunung ?></th>
                    <?php
                    $hitungHari = abs(strtotime($inNaik) - strtotime($inTurun));
                    $unixHari = $hitungHari / 86400;
                    $jumlahHari = intVal($unixHari);
                    ?>
                    <td><?= $jumlahHari; ?></td>
                    <td><?= $inJumlah; ?></td>
                    <?php
                    $totalHarga = $inJumlah * ($jumlahHari * $hargaGunung);
                    ?>
                    <td>Rp. <?= $totalHarga; ?></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12">
            <h2 class="font-weight-bold table-title mb-2">Payment Method</h2>
        </div>
        <div class="row table-title mb-5">
            <div class="col-xs-12 col-md-4">
                <input type="checkbox" name="method[]" value="mandiri">
                <img src="image/logo.png" alt="" height="80px" width="200px">
            </div>
            <div class="col-xs-12 col-md-4">
                <input type="checkbox" name="method[]" value="bca">
                <img src="image/logo.png" alt="" height="80px" width="200px">
            </div>
            <div class="col-xs-12 col-md-4">
                <input type="checkbox" name="method[]" value="indomaret">
                <img src="image/logo.png" alt="" height="80px" width="200px">
            </div>
        </div>
        <div class="col-12">
            <h2 class="font-weight-bold table-title mb-2">Total</h2>
        </div>
        <div class="total">
            <table class="table" width="200">
                <tr>
                    <td>Total</td>
                    <td class="text-right"><?= $totalHarga; ?></td>
                </tr>
                <tr>
                    <td>Admin</td>
                    <td class="text-right">10.000</td>
                </tr>
            </table>
            <hr>
        </div>
        <div class="col-12 mb-2">
            <h1 class="table-title font-weight-bold">Rp <?= ($totalHarga + 10000) ?></h1>
        </div>
        <div class="table-title mb-5">
            <button type="submit" name="button" name="btnCheckout" class="btn btn-daftar">Checkout</button>
        </div>
    </div>
</form>


<?php include "partition/footer.php" ?>

</body>
</html>
