<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Kegunung Indonesia</title>
	<?php include"partition/header.php" ?>
	<link rel="stylesheet" href="css/daftar.css">
</head>
<body>

	<?php include"partition/navbar.php" ?>

	<h1>Daftar</h1>
	<form class="text-center form-daftar" method="post" action="proses/proses-daftar.php">
		<div class="row form-group">
			<div class="col-xs-12 col-lg-12">
				<input type="text" class="form-control" name="usr" placeholder="Username">
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-lg-6">
				<input type="text" class="form-control" name="namaD" placeholder="Nama Depan">
			</div>
			<div class="col-xs-12 col-lg-6">
				<input type="text" class="form-control" name="namaB" placeholder="Nama Belakang">
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-lg-12">
				<input type="text" class="form-control" name="email" placeholder="Email">
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-lg-12">
				<input type="password" class="form-control" name="pwd" placeholder="Password">
			</div>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-lg-12">
				<input type="password" class="form-control" name="repwd" placeholder="Re-Password">
			</div>
		</div>
		<div class="row form-group">
			<button class="btn btn-success button" type="submit" name="daftar">Daftar</button>
		</div>
		<p class="par">Sudah memiliki akun? <a href="#daftar" class="lay">Login</p>
		</form>

		<?php include"partition/footer.php" ?>

	</body>
	</html>
